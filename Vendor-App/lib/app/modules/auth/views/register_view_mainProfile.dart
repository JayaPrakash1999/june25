import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:home_services_provider/app/modules/auth/views/images_upload.dart';

import '../../../../common/helper.dart';
import '../../../../common/ui.dart';
import '../../../models/setting_model.dart';
import '../../../routes/app_routes.dart';
import '../../../services/settings_service.dart';
import '../../global_widgets/block_button_widget.dart';
import '../../global_widgets/circular_loading_widget.dart';
import '../../global_widgets/text_field_widget.dart';
import '../../root/controllers/root_controller.dart';
import '../controllers/auth_controller.dart';

class RegisterMainProfile extends GetView<AuthController>{

  final Setting _settings = Get.find<SettingsService>().setting.value;

  VendorController vendorController = VendorController();
  CategoryController categoryController = CategoryController();
  PermanentAddressCheckBox permanentAddressCheckBox = PermanentAddressCheckBox();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper().onWillPop,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Upload your Profile Details".tr,
            style: Get.textTheme.headline6.merge(TextStyle(color: context.theme.primaryColor)),
          ),
          centerTitle: true,
          backgroundColor: Get.theme.accentColor,
          automaticallyImplyLeading: false,
          elevation: 0,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios, color: Get.theme.primaryColor),
            onPressed: () => {
              // Get.toNamed(Routes.LOGIN)
              Get.find<RootController>().changePageOutRoot(0)
              },
          ),
        ),
        body: Form(
          key: controller.registerProfileFormKey,
          child: ListView(
            primary: true,
            children: [
              Stack(
                alignment: AlignmentDirectional.bottomCenter,
                children: [
                  Container(
                    height: 160,
                    width: Get.width,
                    decoration: BoxDecoration(
                      color: Get.theme.accentColor,
                      borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(color: Get.theme.focusColor.withOpacity(0.2), blurRadius: 10, offset: Offset(0, 5)),
                      ],
                    ),
                    margin: EdgeInsets.only(bottom: 50),
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        children: [
                          Text(
                            _settings.appName,
                            style: Get.textTheme.headline6.merge(TextStyle(color: Get.theme.primaryColor, fontSize: 24)),
                          ),
                          SizedBox(height: 5),
                          Text(
                            "Welcome to the best service provider system!".tr,
                            style: Get.textTheme.caption.merge(TextStyle(color: Get.theme.primaryColor)),
                            textAlign: TextAlign.center,
                          ),
                          // Text("Fill the following credentials to login your account", style: Get.textTheme.caption.merge(TextStyle(color: Get.theme.primaryColor))),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: Ui.getBoxDecoration(
                      radius: 14,
                      border: Border.all(width: 5, color: Get.theme.primaryColor),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      child: Image.asset(
                        'assets/icon/icon.png',
                        fit: BoxFit.cover,
                        width: 100,
                        height: 100,
                      ),
                    ),
                  ),
                ],
              ),
              Obx(() {
                if (controller.loading.isTrue) {
                  return CircularLoadingWidget(height: 300);
                } else {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      TextFieldWidget(
                        labelText: "Full Name".tr,
                        hintText: controller.currentUser.value.name==null?"Name".tr:controller.currentUser.value.name,
                        onSaved: (input) => controller.currentUser.value.name = input,
                        // validator: (input) => input.length < 3 ? "Should be more than 3 characters".tr : null,
                        iconData: Icons.person_outline,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "Email Address".tr,
                        hintText: controller.currentUser.value.email==null?"example@gmail.com".tr:controller.currentUser.value.email,
                        onSaved: (input) => controller.currentUser.value.email = input,
                        // validator: (input) => !input.contains('@') ? "Should be a valid email".tr : null,
                        iconData: Icons.alternate_email,
                        isFirst: false,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "Phone Number".tr,
                        hintText: controller.currentUser.value.phoneNumber==null?"+91 9876543210".tr:controller.currentUser.value.phoneNumber,
                        onSaved: (input) {
                          if (input.startsWith("00")) {
                            input = "+" + input.substring(2);
                          }
                          return controller.currentUser.value.phoneNumber = input;
                        },
                        // validator: (input) {
                        //   return !input.startsWith('\+') && !input.startsWith('00') ? "Should be valid mobile number with country code" : null;
                        // },
                        iconData: Icons.phone_android_outlined,
                        isLast: false,
                        isFirst: false,
                      ),
                      //new data
                      TextFieldWidget(
                        labelText: "Date of Birth (DOB)".tr,
                        keyboardType: TextInputType.datetime,
                        hintText: "DD/MM/YYYY".tr,
                        onSaved: (input) => controller.currentUser.value.dob = input,
                        validator: (input) => input.length < 8 ? "Should be more than 8 characters".tr : null,
                        iconData: Icons.person_pin_circle_rounded,
                        isFirst: true,
                        isLast: false,
                      ),

                      Center(
                        child: Obx( () => Row(
                          children: [
                            SizedBox(width: 35,),
                            Text("Education Status: "),
                            SizedBox(width: 5,),
                            DropdownButton(
                              focusColor:Theme.of(context).focusColor ,
                              icon: Icon(Icons.arrow_drop_down),
                              // icon: Icons(cons.store),
                              hint: Text(
                                'Education Status',
                              ),
                              onChanged: (newValue) {
                                vendorController.setSelected(newValue);
                              },
                              value: vendorController.selected.value,
                              items: vendorController.listType.map((selectedType) {
                                return DropdownMenuItem(
                                  child: new Text(
                                    selectedType,
                                  ),
                                  value: selectedType,
                                );
                              }).toList(),
                            ),
                          ],
                        )
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top:15),
                        child:  Text("** Temporary Address **", textAlign: TextAlign.center,),
                      ),
                      TextFieldWidget(
                        labelText: "Temporary Address".tr,
                        hintText: "Enter Your Temporary Address".tr,
                        onSaved: (input) => controller.currentUser.value.address= input,
                        validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                        iconData: Icons.location_on_outlined,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "City".tr,
                        hintText: "Enter Your City".tr,
                        onSaved: (input) => controller.currentUser.value.temporary_city= input,
                        validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                        iconData: Icons.location_on_outlined,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "Mandal".tr,
                        hintText: "Enter Your Mandal".tr,
                        onSaved: (input) => controller.currentUser.value.temporary_mandal= input,
                        validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                        iconData: Icons.location_on_outlined,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "District".tr,
                        hintText: "Enter Your District".tr,
                        onSaved: (input) => controller.currentUser.value.temporary_district= input,
                        validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                        iconData: Icons.location_on_outlined,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "State".tr,
                        hintText: "Enter Your District".tr,
                        onSaved: (input) => controller.currentUser.value.temporary_state= input,
                        validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                        iconData: Icons.location_on_outlined,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "Country".tr,
                        hintText: "Enter Your District".tr,
                        onSaved: (input) => controller.currentUser.value.temporary_country= input,
                        validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                        iconData: Icons.location_on_outlined,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "Pin code".tr,
                        hintText: "500050".tr,
                        keyboardType: TextInputType.number,
                        onSaved: (input) => controller.currentUser.value.temporary_pinCode = input,
                        validator: (input) => input.length < 6 ? "Should be atleast 6 characters".tr : null,
                        iconData: Icons.location_on_outlined,
                        isFirst: true,
                        isLast: false,
                      ),
                      Container(
                        padding: EdgeInsets.only(top:15,left: 25.0,bottom: 15,right: 25),
                        child:  Row(
                          children: [

                            Obx(()=> Checkbox(value: permanentAddressCheckBox.selectedCheckBox.value, onChanged: (value){
                              print("called");
                              permanentAddressCheckBox.selectedCheckBox(value);
                            }),
                            ),
                            Text("Same as Tempoarary Address ", ),
                          ],
                        ),
                      ),
                      Obx((){

                        if(permanentAddressCheckBox.selectedCheckBox==false)
                        {
                          return TextFieldWidget(
                            labelText: "Permanent Address".tr,
                            hintText: "Enter Your Permanent Address".tr,
                            onSaved: (input) => controller.currentUser.value.permanent_address= input,
                            validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                            iconData: Icons.location_on_outlined,
                            isFirst: true,
                            isLast: false,
                          );}
                        else{
                          return SizedBox();
                        }
                      }),
                      Obx((){

                        if(permanentAddressCheckBox.selectedCheckBox==false)
                        {
                          return TextFieldWidget(
                            labelText: "City".tr,
                            hintText: "Enter Your City".tr,
                            onSaved: (input) => controller.currentUser.value.permanent_city= input,
                            validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                            iconData: Icons.location_on_outlined,
                            isFirst: true,
                            isLast: false,
                          );}
                        else{
                          return SizedBox();
                        }
                      }),
                      Obx((){

                        if(permanentAddressCheckBox.selectedCheckBox==false)
                        {
                          return TextFieldWidget(
                            labelText: "Mandal".tr,
                            hintText: "Enter Your City".tr,
                            onSaved: (input) => controller.currentUser.value.permanent_mandal= input,
                            validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                            iconData: Icons.location_on_outlined,
                            isFirst: true,
                            isLast: false,
                          );}
                        else{
                          return SizedBox();
                        }
                      }),
                      Obx((){

                        if(permanentAddressCheckBox.selectedCheckBox==false)
                        {
                          return TextFieldWidget(
                            labelText: "District".tr,
                            hintText: "Enter Your City".tr,
                            onSaved: (input) => controller.currentUser.value.permanent_district= input,
                            validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                            iconData: Icons.location_on_outlined,
                            isFirst: true,
                            isLast: false,
                          );}
                        else{
                          return SizedBox();
                        }
                      }),
                      Obx((){

                        if(permanentAddressCheckBox.selectedCheckBox==false)
                        {
                          return TextFieldWidget(
                            labelText: "State".tr,
                            hintText: "Enter Your City".tr,
                            onSaved: (input) => controller.currentUser.value.permanent_state= input,
                            validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                            iconData: Icons.location_on_outlined,
                            isFirst: true,
                            isLast: false,
                          );}
                        else{
                          return SizedBox();
                        }
                      }),
                      Obx((){

                        if(permanentAddressCheckBox.selectedCheckBox==false)
                        {
                          return TextFieldWidget(
                            labelText: "Country".tr,
                            hintText: "Enter Your City".tr,
                            onSaved: (input) => controller.currentUser.value.permanent_country= input,
                            validator: (input) => input.length < 7 ? "Should be more than 7 characters".tr : null,
                            iconData: Icons.location_on_outlined,
                            isFirst: true,
                            isLast: false,
                          );}
                        else{
                          return SizedBox();
                        }
                      }),
                      Obx((){
                        if(permanentAddressCheckBox.selectedCheckBox==false)
                        {
                          return TextFieldWidget(
                            labelText: "Permanent Pin code".tr,
                            hintText: "500050".tr,
                            keyboardType: TextInputType.number,
                            onSaved: (input) => controller.currentUser.value.permanent_pinCode = input,
                            validator: (input) => input.length < 6 ? "Should be atleast 6 characters".tr : null,
                            iconData: Icons.location_on_outlined,
                            isFirst: true,
                            isLast: false,
                          );
                        }
                        else{
                          return SizedBox();
                        }
                      }),
                      TextFieldWidget(
                        labelText: "Enter GST Number".tr,
                        hintText: "XXXX XXXX XXXX XXX".tr,
                        onSaved: (input) => controller.currentUser.value.gstNumber= input,
                        validator: (input) => input.length == 15 ? "Should be 15 characters".tr : null,
                        iconData: Icons.contact_page_sharp,
                        isFirst: true,
                        isLast: false,
                      ),
                      Container(
                        padding: EdgeInsets.only(top:15),
                        child: Text("**Document Numbers**", textAlign: TextAlign.center,),
                      ),
                      TextFieldWidget(
                        labelText: "Enter Aadhar Number".tr,
                        hintText: "XXXXX XXXXX".tr,
                        keyboardType: TextInputType.number,
                        onSaved: (input) => controller.currentUser.value.aadharNumber= input,
                        validator: (input) => input.length == 10 ? "Should be 10 characters".tr : null,
                        iconData: Icons.contact_page_sharp,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "Enter PAN Number".tr,
                        hintText: "XXXX XXXX XXXX".tr,
                        onSaved: (input) => controller.currentUser.value.panNumber = input,
                        validator: (input) => input.length == 12 ? "Should be 12 characters".tr : null,
                        iconData: Icons.contact_page_sharp,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "Enter Driving Licence Number".tr,
                        hintText: "XXXXXX".tr,
                        onSaved: (input) => controller.currentUser.value.drivingLicence = input,
                        validator: (input) => input.length < 4 ? "Should be more than 4 characters".tr : null,
                        iconData: Icons.contact_page_sharp,
                        isFirst: true,
                        isLast: false,
                      ),
                      Container(
                        padding: EdgeInsets.only(top:15),
                        child:  Text("**Bank account details**", textAlign: TextAlign.center,),
                      ),


                      //update same as pincode
                      TextFieldWidget(
                        labelText: "Bank Account Number".tr,
                        hintText: "XXXXXXXXXXXXX".tr,
                        keyboardType: TextInputType.number,
                        onSaved: (input) => controller.currentUser.value.aadharNumber = input,
                        validator: (input) => input.length < 6 ? "Should be atleast 6 characters".tr : null,
                        iconData: Icons.account_balance,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "Account Holder Name".tr,
                        hintText: "Account Holder Nam".tr,
                        onSaved: (input) => controller.currentUser.value.bankAccountName = input,
                        validator: (input) => input.length < 4 ? "Should be more than 4 characters".tr : null,
                        iconData: Icons.account_balance,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "IFSC Code".tr,
                        hintText: "XXXX XXXXX".tr,
                        onSaved: (input) => controller.currentUser.value.ifsc = input,
                        validator: (input) => input.length < 8 ? "Should be more than 8 characters".tr : null,
                        iconData: Icons.account_balance,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "UPI ID".tr,
                        hintText: "XXXXXXXX@XXXX".tr,
                        onSaved: (input) => controller.currentUser.value.upiId= input,
                        validator: (input) => input.length < 8 ? "Should be more than 8 characters".tr : null,
                        iconData: Icons.account_balance,
                        isFirst: true,
                        isLast: false,
                      ),
                      TextFieldWidget(
                        labelText: "REFFERAL CODE".tr,
                        hintText: "XXXXXXX".tr,
                        onSaved: (input) => controller.currentUser.value.refferal= input,
                        validator: (input) => input.length < 8 ? "Should be more than 8 characters".tr : null,
                        iconData: Icons.account_circle_outlined,
                        isFirst: true,
                        isLast: false,
                      ),
                      SizedBox(
                        width: 500,
                        child: ElevatedButton(onPressed:(){
                          Navigator.push(context, MaterialPageRoute(builder: (BuildContext){
                            return ImagesUpload();
                          }));
                        }, child: 
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("click here to upload required documents"),
                                Icon(Icons.arrow_right)
                              ],
                            ),
                         ),
                      ),
                      Container(
                          padding: EdgeInsets.only(top:15),
                          child: Text("Selected Vendor type is : "+controller.currentUser.value.vendorType.toString(), textAlign: TextAlign.center,)
                      ),
                      Center(
                        child: Obx( () => Row(
                          children: [
                            SizedBox(width: 15,),
                            Text("Select the Category type: "),
                            SizedBox(width: 5,),
                            DropdownButton(
                              focusColor:Theme.of(context).focusColor ,
                              // icon: Icon(Icons.store_sharp),
                              // icon: Icons(cons.store),
                              hint: Text(
                                'Categories Available',
                              ),
                              onChanged: (newValue) {
                                categoryController.setSelected(newValue);
                              },
                              value: categoryController.selected.value,
                              items: categoryController.listType.map((selectedType) {
                                return DropdownMenuItem(
                                  child: SizedBox(
                                    width: 130,
                                    child: new Text(
                                      selectedType,
                                    ),
                                  ),
                                  value: selectedType,
                                );
                              }).toList(),
                            ),
                          ],
                        )
                        ),
                      ),
                      // Container(
                      //   padding: EdgeInsets.only(top:10),
                      //   child:  Text("**Uploads**", textAlign: TextAlign.center,),
                      // ),


                    ],
                  );
                }
              })
            ],
          ),
        ),
        bottomNavigationBar: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              direction: Axis.vertical,
              children: [
                SizedBox(
                  width: Get.width,
                  child: BlockButtonWidget(
                    onPressed: () {
                      controller.register();
                      //Get.offAllNamed(Routes.PHONE_VERIFICATION);
                    },
                    color: Get.theme.accentColor,
                    text: Text(
                      "Register".tr,
                      style: Get.textTheme.headline6.merge(TextStyle(color: Get.theme.primaryColor)),
                    ),
                  ).paddingOnly(top: 15, bottom: 5, right: 20, left: 20),
                ),
                // TextButton(
                //   onPressed: () {
                //     Get.offAllNamed(Routes.LOGIN);
                //   },
                //   child: Text("You already have an account?".tr),
                // ).paddingOnly(bottom: 10),
              ],
            ),
          ],
        ),
      ),
    );
  }
}


class VendorController extends GetxController {
  // It is mandatory initialize with one value from listType
  final selectedPayment=1350.obs;
  final selected = "No formal education".obs;
  var listType= <String>['No formal education','Secondary', "Bachelor's degree","Master's degree", "Doctorate or higher"];
  void setSelected(String value){
    selected.value = value;
  }
}
class PermanentAddressCheckBox extends GetxController{
  RxBool selectedCheckBox=true.obs;
  void setSelected(bool value)
  {
    print("called function");
    selectedCheckBox.value=!selectedCheckBox.value;
  }
}
class CategoryController extends GetxController {
  final selected = "ELECTRICAL SERVICES".obs;
  var listType= <String>['ELECTRICAL SERVICES','CARPENTRY', "PLUMBING AND SANITARY","PAINTING SERVICES", "CLEANING FOR HOME OR OFFICE","BEAUTY AND SALON SERVICES","TRANSPORTATION","DOCTORS","LAWYERS","FINANCIAL SERVICES","EVENT ORGANIZATIONS","PHOTOGRAPHER","MEDICAL HEALTH SERVICES","PET CARE SERVICE","FARMERS PLATFORM","DAIRY PRODUCTS SUPPLY","PRINTING PRESS SERVICES","LADIES AND GENTS TAILORS","PLANT NURSERY","BROKERS AND AGENTS","VEHICLE WASH SERVICES"];
  void setSelected(String value){
    selected.value = value;
  }
}