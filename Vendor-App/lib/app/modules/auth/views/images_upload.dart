// import 'dart:convert';
// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:multi_image_picker/multi_image_picker.dart';

// class ImagesUpload extends StatefulWidget {
//   @override
//   _ImagesUploadState createState() => _ImagesUploadState();
// }

// class _ImagesUploadState extends State<ImagesUpload> {
//   List<Asset> images = <Asset>[];
//   String _error = 'No Error Dectected';

//   @override
//   void initState() {
//     super.initState();
//   }

//   Widget buildGridView() {
//     return GridView.count(
//       crossAxisCount: 3,
//       children: List.generate(images.length, (index) {
//         Asset asset = images[index];
//         return AssetThumb(
//           asset: asset,
//           width: 300,
//           height: 300,
//         );
//       }),
//     );
//   }

//   Future<void> loadAssets() async {
//     List<Asset> resultList = <Asset>[];
//     String error = 'No Error Detected';

//     try {
//       resultList = await MultiImagePicker.pickImages(
//         maxImages: 8,
//         enableCamera: true,
//         selectedAssets: images,
//         cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
//         // materialOptions: MaterialOptions(
//         //   actionBarColor: "#abcdef",
//         //   actionBarTitle: "Upload mentioned documents",
//         //   allViewTitle: "All Photos",
//         //   // useDetailsView: false,
//         //   // selectCircleStrokeColor: olors,
//         // ),
//       );
//     } on Exception catch (e) {
//       error = e.toString();
//     }

//     // If the widget was removed from the tree while the asynchronous platform
//     // message was in flight, we want to discard the reply rather than calling
//     // setState to update our non-existent appearance.
//     if (!mounted) return;

//     setState(() {
//       images = resultList;
//       _error = error;
//     });
//   }

    
//   // getImageFile(String path) async{
//   //   final file= File(path);
//   //   return file;
//   // }

//   // _submit() async {
//   //   for (int i = 0; i < images.length; i++) {
//   //       var path2 = await FlutterAbsolutePath.getAbsolutePath(images[i].identifier);
//   //       var file = await getImageFileFromAsset(path2);
//   //       var base64Image = base64Encode(file.readAsBytesSync());
//   //       files.add(base64Image);
//   //       var data = {
//   //       "files": files,
//   //       };
//   //       try {
//   //           var response = await http.post(data, 'url')
//   //           var body = jsonDecode(response.body);
//   //           print(body);
//   //           if (body['msg'] == "Success!") {
//   //           print('posted successfully!');
//   //       } else {
//   //           _showToast(context, body['msg']);
//   //       }
//   //       } catch (e) {
//   //          return e.message;
//   //       }
//   //   }
//   // }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         // appBar: AppBar(
//         //   title: const Text('Plugin example app'),
//         // ),
//         appBar: AppBar(
//           title: Text(
//             "Upload your Profile Details".tr,
//             style: Get.textTheme.headline6.merge(TextStyle(color: context.theme.primaryColor)),
//           ),
//           centerTitle: true,
//           backgroundColor: Get.theme.accentColor,
//           automaticallyImplyLeading: false,
//           elevation: 0,
//           // leading: new IconButton(
//           //   icon: new Icon(Icons.arrow_back_ios, color: Get.theme.primaryColor),
//           //   onPressed: () => {
//           //     // Get.toNamed(Routes.LOGIN)
//           //     // Get.find<RootController>().changePageOutRoot(0)
//           //     // Navigaot.pop
//           //     },
//           // ),
//         ),
//         body: Column(
//           children: <Widget>[
//             SizedBox(
//               child: Text("Please upload the folowing images: "),
//             ),
//             Center(child: Text('Error: $_error')),
//             ElevatedButton(
//               child: Text("Pick images"),
//               onPressed: loadAssets,
//             ),
//             Expanded(
//               child: buildGridView(),
//             ),
//             SizedBox(height:100),
//             images.length>4?ElevatedButton(
//               child: Text("Upload"),
//               onPressed: loadAssets,
//             ):SizedBox(),
//           ],
//         ),
//     );
//   }
// }





import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:multi_image_picker/multi_image_picker.dart';

class ImagesUpload extends StatefulWidget {
  @override
  _ImagesUploadState createState() => _ImagesUploadState();
}

class _ImagesUploadState extends State<ImagesUpload> {
  List<Asset> images = <Asset>[];
  List files=[];
  String _error = 'No Error Dectected';

  @override
  void initState() {
    super.initState();
  }
  getImageFile(String path) async{
    final file= File(path);
    return file;
  }

  getImageFileFromAsset(String path) async{
    final file= File(path);
    return file;
  }

  _submit() async {
    print("called");
    for (int i = 0; i < images.length; i++) {
        var path2 = await FlutterAbsolutePath.getAbsolutePath(images[i].identifier);
        var file = await getImageFileFromAsset(path2);
        var base64Image = base64Encode(file.readAsBytesSync());
        files.add(base64Image);
        // var data = {
        // "files": files,
        // "mobile_number":"901059086"
        // };
        try {
          print("trying");
          var request= http.MultipartRequest('POST',Uri.parse("https://merakibros.com/admin/public/api/upload/files"));
          request.files.add(await http.MultipartFile.fromPath('picture$i', path2));//try with file too
          request.fields['mobile_number']="9010590693";
          var res= await request.send();
          print("done");
          print(res.statusCode);
          if(res.statusCode==200)
          {
            Get.back();
          }
          else{
            Fluttertoast.showToast(msg: "Error in uploading documents");
          }
            // var response = await http.post(
            //   Uri.parse("https://merakibros.com/admin/public/api/upload/files"),
            //   headers: {
            //     'Content-Type': 'application/json', 
            //     'Accept': 'application/json',
            //   },
            //   body: json.encode({
            //     "aadhar":file,
            //     "mobile_number":"9010590693"
            //   }) );
            //   print("done");
            // var body = jsonDecode(response.body);
            // print(body);
            // if (body['msg'] == "Success!") {
            // print('posted successfully!');
        // } else {
        //     Fluttertoast.showToast(msg:body['msg']);
        // }
        } catch (e) {
          // print(e);
           return e.message;
        }
    }
}
  Widget buildGridView() {
    return GridView.count(
      crossAxisCount: 3,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return AssetThumb(
          asset: asset,
          width: 300,
          height: 300,
        );
      }),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = <Asset>[];
    String error = 'No Error Detected';
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 10,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Upload mentioned documents",
          allViewTitle: "All Photos",
          // useDetailsView: false,
          // selectCircleStrokeColor: olors,
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      _error = error;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   title: const Text('Plugin example app'),
        // ),
        appBar: AppBar(
          title: Text(
            "Upload your Profile Details".tr,
            style: Get.textTheme.headline6.merge(TextStyle(color: context.theme.primaryColor)),
          ),
          centerTitle: true,
          backgroundColor: Get.theme.accentColor,
          automaticallyImplyLeading: false,
          elevation: 0,
          // leading: new IconButton(
          //   icon: new Icon(Icons.arrow_back_ios, color: Get.theme.primaryColor),
          //   onPressed: () => {
          //     // Get.toNamed(Routes.LOGIN)
          //     // Get.find<RootController>().changePageOutRoot(0)
          //     // Navigaot.pop
          //     },
          // ),
        ),
        body: Column(
          children: <Widget>[
            SizedBox(
              child: Text("Please upload the folowing images: "),
            ),
            _error!="No Error Detected"?Center(child: Text('Error: $_error')):SizedBox(),
            ElevatedButton(
              child: Text("Pick images"),
              onPressed: loadAssets,
            ),
            Expanded(
              child: buildGridView(),
            ),
            SizedBox(height:100),
            images.length>4?ElevatedButton(
              child: Text("Upload"),
              onPressed: (){
                _submit();
              },
            ):SizedBox(),
          ],
        ),
    );
  }
}