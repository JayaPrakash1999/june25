import 'package:flutter/material.dart';
import 'package:flutter_html/html_parser.dart';
import 'package:get/get.dart';

import '../../../../common/helper.dart';
import '../../../../common/ui.dart';
import '../../../models/setting_model.dart';
import '../../../routes/app_routes.dart';
import '../../../services/settings_service.dart';
import '../../global_widgets/block_button_widget.dart';
import '../../global_widgets/circular_loading_widget.dart';
import '../../global_widgets/text_field_widget.dart';
import '../../root/controllers/root_controller.dart';
import '../controllers/auth_controller.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';

class RegisterView extends GetView<AuthController> {
  final Setting _settings = Get.find<SettingsService>().setting.value;
  Razorpay _razorpay;
  VendorController vendorController = VendorController();

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    Fluttertoast.showToast(
        msg: "SUCCESS: " + response.paymentId);
    Get.offAllNamed(Routes.REGISTER_MAINPROFILE);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName);
  }

  void openCheckout() async {
    print("in OC");
    var options = {
      'key': 'rzp_live_UZTeEmfmIeNp2m',
      'amount': 100,
      //  vendorController.selectedPayment*100,
      'name': controller.currentUser.value.name==null?
      'Shaiq':controller.currentUser.value.name ,
      'description': 'Payment for Subscription',
      'prefill': {'contact': controller.currentUser.value.phoneNumber==null?
      '8888888888':controller.currentUser.value.phoneNumber ,
        'email': controller.currentUser.value.email==null?
        'test@rvendor.com':controller.currentUser.value.email },
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint(e.toString());
    }
  }




  @override
  Widget build(BuildContext context) {
    return StatefulWrapper(
      onInit: (){
        _razorpay = Razorpay();
        _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
        _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
        _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
      },
      child: WillPopScope(
        onWillPop: Helper().onWillPop,
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              "Register".tr,
              style: Get.textTheme.headline6.merge(TextStyle(color: context.theme.primaryColor)),
            ),
            centerTitle: true,
            backgroundColor: Get.theme.accentColor,
            automaticallyImplyLeading: false,
            elevation: 0,
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back_ios, color: Get.theme.primaryColor),
              onPressed: () => {
                
                Get.find<RootController>().changePageOutRoot(0)},
            ),
          ),
          body: Form(
            key: controller.registerFormKey,
            child: ListView(
              primary: true,
              children: [
                Stack(
                  alignment: AlignmentDirectional.bottomCenter,
                  children: [
                    Container(
                      height: 130,
                      width: Get.width,
                      decoration: BoxDecoration(
                        color: Get.theme.accentColor,
                        borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(color: Get.theme.focusColor.withOpacity(0.2), blurRadius: 10, offset: Offset(0, 5)),
                        ],
                      ),
                      margin: EdgeInsets.only(bottom: 30),
                      child: Padding(
                        padding: const EdgeInsets.only(left:20,right:20,bottom:20),
                        child: Column(
                          children: [
                            Text(
                              _settings.appName,
                              style: Get.textTheme.headline6.merge(TextStyle(color: Get.theme.primaryColor, fontSize: 24)),
                            ),
                            SizedBox(height: 5),
                            Text(
                              "Welcome to the best service provider system!".tr,
                              style: Get.textTheme.caption.merge(TextStyle(color: Get.theme.primaryColor)),
                              textAlign: TextAlign.center,
                            ),
                            // Text("Fill the following credentials to login your account", style: Get.textTheme.caption.merge(TextStyle(color: Get.theme.primaryColor))),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      decoration: Ui.getBoxDecoration(
                        radius: 14,
                        border: Border.all(width: 5, color: Get.theme.primaryColor),
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        child: Image.asset(
                          'assets/icon/icon.png',
                          fit: BoxFit.cover,
                          width: 100,
                          height: 100,
                        ),
                      ),
                    ),
                  ],
                ),
                Obx(() {
                  if (controller.loading.isTrue) {
                    return CircularLoadingWidget(height: 300);
                  } else {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        TextFieldWidget(
                          labelText: "Full Name".tr,
                          hintText: "Name".tr,
                          onChanged: (input) => controller.currentUser.value.name = input,
                          validator: (input) => input.length < 3 ? "Should be more than 3 characters".tr : null,
                          iconData: Icons.person_outline,
                          isFirst: true,
                          isLast: false,
                        ),
                        TextFieldWidget(
                          labelText: "Email Address".tr,
                          hintText: "example@gmail.com".tr,
                          onChanged: (input) => controller.currentUser.value.email = input,
                          validator: (input) => !input.contains('@') ? "Should be a valid email".tr : null,
                          iconData: Icons.alternate_email,
                          isFirst: false,
                          isLast: false,
                        ),
                        TextFieldWidget(
                          labelText: "Phone Number".tr,
                          hintText: "+91 9876543210".tr,
                          onChanged: (input) {
                            if (input.startsWith("00")) {
                              input = "+" + input.substring(2);
                            }
                            return controller.currentUser.value.phoneNumber = input;
                          },
                          validator: (input) {
                            return !input.startsWith('\+') && !input.startsWith('00') ? "Should be valid mobile number with country code" : null;
                          },
                          iconData: Icons.phone_android_outlined,
                          isLast: false,
                          isFirst: false,
                        ),
                        Obx(() {
                          return TextFieldWidget(
                            labelText: "Password".tr,
                            hintText: "••••••••••••".tr,
                            onChanged: (input) => controller.currentUser.value.password = input,
                            validator: (input) => input.length < 3 ? "Should be more than 3 characters".tr : null,
                            obscureText: controller.hidePassword.value,
                            iconData: Icons.lock_outline,
                            keyboardType: TextInputType.visiblePassword,
                            isLast: true,
                            isFirst: false,
                            suffixIcon: IconButton(
                              onPressed: () {
                                controller.hidePassword.value = !controller.hidePassword.value;
                              },
                              color: Theme.of(context).focusColor,
                              icon: Icon(controller.hidePassword.value ? Icons.visibility_outlined : Icons.visibility_off_outlined),
                            ),
                          );
                        }),
                        Center(
                            child: Obx( () => SizedBox(
                              width: 320,
                              child: DropdownButtonFormField(
                                  focusColor:Theme.of(context).focusColor ,
                                  icon: Icon(Icons.arrow_drop_down),
                                  // icon: Icon(Icons.store),
                                  hint: Text(
                                    'Vendor Type',
                                  ),
                                  onChanged: (newValue) {
                                    vendorController.setSelected(newValue);
                                  },
                                  value: vendorController.selected.value,
                                  items: vendorController.listType.map((selectedType) {
                                    return
                                      DropdownMenuItem(
                                        child: new Text(
                                          selectedType,
                                        ),
                                        value: selectedType,
                                      );
                                  }).toList(),
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(borderRadius:
                                    BorderRadius.horizontal(
                                        left: Radius.circular(50),
                                        right: Radius.circular(50))
                                    ),
                                    contentPadding: EdgeInsets.all(15),
                                  )
                              ),
                            ),
                            )),
                        SizedBox(height:10),
                        Center(
                          child:Obx(()=>
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text("Subscription charges for "+vendorController.selected.toString()+": ₹ "),
                                      Text(vendorController.selected.toString()=="Company"?"3000":vendorController.selectedPayment.toString()),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text("Discount : "),
                                      Text(vendorController.selected.toString()=="Company"?"1500":"0"),
                                    ],
                                  ),
                                  Text("Total : "+vendorController.selectedPayment.toString())
                                ],
                              )
                          ),
                        )
                      ],
                    );
                  }
                })
              ],
            ),
          ),
          bottomNavigationBar: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                direction: Axis.vertical,
                children: [
                  SizedBox(
                    width: Get.width,
                    child: BlockButtonWidget(
                      onPressed: () {
                        controller.currentUser.value.vendorType=vendorController.selected.value;
                        Get.focusScope.unfocus();
                        if (controller.registerFormKey.currentState.validate()) {
                        controller.registerFormKey.currentState.save();
                        controller.loading.value = true;
                        vendorController.selectedPayment==0?
                        Get.offAllNamed(Routes.REGISTER_MAINPROFILE):
                        openCheckout();
                        controller.loading.value = false;
                        }
                        // controller.register();
                        // Get.offAllNamed(Routes.PHONE_VERIFICATION);
                      },
                      color: Get.theme.accentColor,
                      text: Text(
                        "Make Payment".tr,
                        style: Get.textTheme.headline6.merge(TextStyle(color: Get.theme.primaryColor)),
                      ),
                    ).paddingOnly(top: 15, bottom: 5, right: 20, left: 20),
                  ),
                  TextButton(
                    onPressed: () {
                      Get.offAllNamed(Routes.LOGIN);
                    },
                    child: Text("You already have an account?".tr),
                  ).paddingOnly(bottom: 10),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}


class VendorController extends GetxController {
  // It is mandatory initialize with one value from listType
  final selectedPayment=1350.obs;
  final selected = "Individual".obs;
  var listType= <String>['Individual','Proprietor', 'Company','Partnership', 'Volunteer', 'NGOs or Charity', 'Driver'];
  void setSelected(String value){
    selected.value = value;
    if(selected.value=="Individual"||selected.value=="Proprietor"||selected.value=="Partnership"||selected.value=="Volunteer"||selected.value=="Driver")
    {
      selectedPayment.value=1350;
    }
    else if(selected.value=="Company"){
      selectedPayment.value=1500;
    }
    else{
      selectedPayment.value=1350;
    }
    //  switch(value){
    //    case 'Individual':{selectedPayment.value=1500;
    //    break;
    //    }
    //    case 'Proprietor':{
    //      selectedPayment.value="1500"
    //    }
    //  }
  }

}


class StatefulWrapper extends StatefulWidget {
  final Function onInit;
  final Widget child;  const StatefulWrapper({@required this.onInit, @required this.child});
  @override
  _StatefulWrapperState createState() => _StatefulWrapperState();
}

class _StatefulWrapperState extends State<StatefulWrapper> {
  @override
  void initState() {
    if(widget.onInit != null) {
      widget.onInit();
    }
    super.initState();
  }  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}