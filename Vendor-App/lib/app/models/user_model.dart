import 'media_model.dart';
import 'parents/model.dart';

class User extends Model {
  String name;
  String email;
  String password;
  Media avatar;
  String apiToken;
  String deviceToken;
  String phoneNumber;
  bool verifiedPhone;
  String verificationId;
  String address;
  String bio;
  String vendorType;
  String dob;
  String panNumber;
  String qualification;
  String temporary_pinCode;
  String gstNumber;
  String drivingLicence;
  String bankNumber;
  String bankAccountName;
  String ifsc;
  String upiId;
  String aadharNumber;
  String refferal;
  String temporary_city;
  String temporary_mandal;
  String temporary_district;
  String temporary_state;
  String temporary_country;
  String permanent_address;
  String permanent_pinCode;
  String permanent_city;
  String permanent_district;
  String permanent_state;
  String permanent_country;
  String permanent_mandal;


  bool auth;

  User({this.name, this.email, this.password, this.apiToken, this.deviceToken,
    this.phoneNumber, this.verifiedPhone, this.verificationId, this.address,
    this.bio, this.vendorType, this.dob, this. panNumber, this.permanent_address,
    this.temporary_country, this.temporary_state, this.temporary_district,
    this.temporary_mandal, this.temporary_city, this.qualification,
    this.temporary_pinCode,this.gstNumber,this.drivingLicence,this.bankNumber,
    this.bankAccountName, this.ifsc,this.upiId, this.aadharNumber, this.refferal,
    this.permanent_pinCode, this.permanent_city, this.permanent_district,
    this.permanent_state, this.permanent_country, this.permanent_mandal});
//need to update below
  User.fromJson(Map<String, dynamic> json) {
    name = stringFromJson(json, 'name');
    email = stringFromJson(json, 'email');
    apiToken = stringFromJson(json, 'api_token');
    deviceToken = stringFromJson(json, 'device_token');
    phoneNumber = stringFromJson(json, 'phone_number');
    verifiedPhone = boolFromJson(json, 'phone_verified_at');
    avatar = mediaFromJson(json, 'avatar');
    auth = boolFromJson(json, 'auth');
    try {
      address = json['custom_fields']['address']['view'];
    } catch (e) {
      address = stringFromJson(json, 'address');
    }
    try {
      bio = json['custom_fields']['bio']['view'];
    } catch (e) {
      bio = stringFromJson(json, 'bio');
    }
    super.fromJson(json);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    if (password != null && password != '') {
      data['password'] = this.password;
    }
    data['api_token'] = this.apiToken;
    if (deviceToken != null) {
      data["device_token"] = deviceToken;
    }
    data["phone_number"] = phoneNumber;
    if (verifiedPhone != null && verifiedPhone) {
      data["phone_verified_at"] = DateTime.now().toLocal().toString();
    }
    data["address"] = address;
    data["bio"] = bio;
    if (avatar != null) {
      data["media"] = [avatar.toJson()];
    }
    data['auth'] = this.auth;
    return data;
  }

  Map toRestrictMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["email"] = email;
    map["name"] = name;
    map["thumb"] = avatar.thumb;
    map["device_token"] = deviceToken;
    return map;
  }

  @override
  bool operator ==(Object other) =>
      super == other &&
      other is User &&
      runtimeType == other.runtimeType &&
      name == other.name &&
      email == other.email &&
      password == other.password &&
      avatar == other.avatar &&
      apiToken == other.apiToken &&
      deviceToken == other.deviceToken &&
      phoneNumber == other.phoneNumber &&
      verifiedPhone == other.verifiedPhone &&
      verificationId == other.verificationId &&
      address == other.address &&
      bio == other.bio &&
      auth == other.auth;

  @override
  int get hashCode =>
      super.hashCode ^
      name.hashCode ^
      email.hashCode ^
      password.hashCode ^
      avatar.hashCode ^
      apiToken.hashCode ^
      deviceToken.hashCode ^
      phoneNumber.hashCode ^
      verifiedPhone.hashCode ^
      verificationId.hashCode ^
      address.hashCode ^
      bio.hashCode ^
      auth.hashCode;
}
